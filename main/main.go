package main

import (
	"net/http"
	"log"
	"flag"
)
var addr = flag.String("addr", ":1374", "http service address")

func main() {
	server := NewServer("/entry")
	go server.run()
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(server, w, r)
	})
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
