package main


import (
	"log"
	//"strconv"
	"strconv"
)

// Chat server.
type Server struct {
	path string
	clients []*Client
	addClient chan *Client
	removeClient chan *Client

	// Message
	sendAll chan *Message
	messages []*Message

	// User List
	sendUserList chan *Message
	userListMsg Message
	userlist map[string]string

	// id for set the default username
	idUserConnected int

}

// Create new main server.
func NewServer(path string) *Server {
	clients := make([]*Client, 0)
	addClient := make(chan *Client)
	removeClient := make(chan *Client)

	sendAll := make(chan *Message)
	messages := make([]*Message, 0)

	sendUserList := make(chan *Message)

	// Initiate User list Message
	userListMsg := Message{
		Type: "userlist",
		Author: "",
		Body: "",
		UserList: map[string]string{},
	}
	userlist := map[string]string{}

	var idUserConnected = 0
	return &Server{path, clients, addClient, removeClient, sendAll, messages, sendUserList, userListMsg, userlist, idUserConnected }
}

func (self *Server) AddClient() chan<- *Client {
	return (chan<- *Client)(self.addClient)
}

func (self *Server) RemoveClient() chan<- *Client {
	return (chan<- *Client)(self.removeClient)
}

func (self *Server) SendAll() chan<- *Message {
	return (chan<-*Message)(self.sendAll)
}

func (self *Server) SendUserList() chan<- *Message {
	return (chan<-*Message)(self.sendUserList)
}

//
//func (self *Server) Messages() []*Message {
//	msgs := make([]*Message, len(self.messages))
//	copy(msgs, self.messages)
//	return msgs
//}

// Set the User list Message with the User list (Slice)
func (self *Server) UserListMsg() {
	self.userListMsg.UserList = self.userlist
	log.Println("update userList", self.userListMsg)
}


func (self *Server) ChangeNickname(nickname string, c *Client) bool {
	for i := range self.userlist {

		if i == nickname {
			return false
		}
	}
	self.userlist[c.String()] = nickname
	log.Println("nickname change userlist", self.userlist)
	self.UserListMsg()
	log.Println("userlistMsg", self.userListMsg)

	return true
}



// Listen and serve.
// It serves client connection and broadcast request.
func (server *Server) run() {
	for {
		select {

		// Add new a client
		case c := <-server.addClient:
			log.Println("Added new client")

		//Increase the users counter
			server.idUserConnected++

		// set the id to the Client
			c.id = server.idUserConnected


		// add client to the server client list
			server.clients = append(server.clients, c)

		// add default username to the server user list
			server.userlist[c.String()] = "user" + strconv.Itoa(server.idUserConnected)

		// update the user list Message with the userlist update
			server.UserListMsg()

			idClient := Message{
				Type: "id",
				Id: c.String(),
				Author: "",
				Body: "",
				UserList: map[string]string{},
			}
			c.Write() <- &idClient

			for _, msg := range server.messages {
				c.Write() <- msg
			}
			log.Println("userlist:", server.userlist)
			log.Println("userlistmsg:", server.userListMsg)

			if len(server.userListMsg.UserList) > 0 {

				log.Println("Send userList:", &server.userListMsg)
				for _, c := range server.clients {
					c.Write() <- &server.userListMsg
				}
			}
			log.Println("Now", len(server.clients), "clients connected.")


		// remove a client
		case c := <-server.removeClient:
			log.Println("Remove client")
			for i := range server.clients {
				if server.clients[i] == c {
					// remove client from the server client list
					server.clients = append(server.clients[:i], server.clients[i + 1:]...)

					// remove user from the server user list
					delete(server.userlist, c.String());

					log.Println("Now", len(server.clients), "clients connected.")
					if len(server.userListMsg.UserList) > 0 {

						log.Println("Send userList:", &server.userListMsg)
						for _, c := range server.clients {
							c.Write() <- &server.userListMsg
						}
					}
					break
				}
			}


		// broadcast message for all clients
		case msg := <-server.sendAll:
			log.Println("Send all:", msg)
			server.messages = append(server.messages, msg)
			for _, c := range server.clients {
				c.Write() <- msg
			}

		// Send  UserList to all client
		case msg := <-server.sendUserList:
			log.Println("Send userList to all client:", msg)
			for _, c := range server.clients {
				c.Write() <- &server.userListMsg
			}
		}
	}
}
