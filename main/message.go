package main

type Message struct {
	Type string `json:"type"`
	Id string `json:"id"`
	Author string `json:"author"`
	Body string `json:"body"`
	UserList map[string]string `json:"userlist"`
//Date string `json:"date"`
	Color string `json:"color"`
}

func (self *Message) String() string {
	return self.Author + " says " + self.Body + " with " + self.Color + " color"
}