package main

import (
	"github.com/gorilla/websocket"
	"net/http"
	"fmt"
	"log"
)


// Chat client.
type Client struct {
	conn *websocket.Conn
	server *Server
	ch chan *Message
	done chan bool
	id int
}

// write channel buffer size
const channelBufSize = 1000



var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func (c Client) String() string {
	return fmt.Sprintf("%b", c.id)
}


//// Create new main client.
//func NewClient(conn *websocket.Conn, server *Server) *Client {
//
//	if conn == nil {
//		panic("conn cannot be nil")
//	} else if server == nil {
//		panic("server cannot be nil")
//	}
//
//	ch := make(chan *Message, channelBufSize)
//	done := make(chan bool)
//
//	return &Client{conn, server, ch, done}
//}

// Get websocket connection.
func (self *Client) Conn() *websocket.Conn {
	return self.conn
}

// Get Write channel
func (self *Client) Write() chan<-*Message {
	return (chan<-*Message)(self.ch)
}

// Get done channel.
func (self *Client) Done() chan<-bool {
	return (chan<-bool)(self.done)
}

// Listen Write and Read request via chanel
func (self *Client) Listen() {
	go self.listenWrite()
	self.listenRead()
}

// Listen write request via chanel
//  SERVER ========> CLIENT

func (self *Client) listenWrite() {
	log.Println("Listening write to client")
	for {
		select {

		// send message to the client
		case msg := <-self.ch:
			log.Println("Send:", msg)
			err := websocket.WriteJSON(self.conn, msg)
			if err != nil {
				log.Println("remove 1:", err)
				self.server.RemoveClient() <- self
				self.done <- true // for listenRead method
				return
			}


		// receive done request
		case <-self.done:
			log.Println("remove 2")
			self.server.RemoveClient() <- self
			self.done <- true // for listenRead method
			return
		}
	}
}

// Listen read request via chanel
//  CLIENT ========> SERVER
func (self *Client) listenRead() {
	log.Println("Listening read from client")
	for {
		select {

		// receive done request
		case <-self.done:
			log.Println("remove 3")
			self.server.RemoveClient() <- self
			self.done <- true // for listenWrite method
			return

		// read data from websocket connection
		default:
			var msg Message
			err := websocket.ReadJSON(self.conn, &msg)
			if err != nil {
				log.Println("remove 4")
				self.server.RemoveClient() <- self
				self.done<-true
			} else if msg.Type == "changeNickname" {
				log.Println("changeNickname to: ", msg)
				self.server.ChangeNickname(msg.Author, self)
				log.Println("sendUserlist: ")
				self.server.SendUserList() <- &msg

			} else {
				self.server.SendAll() <- &msg
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func serveWs(server *Server, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &Client{server: server, conn: conn, ch: make(chan *Message)}
	client.server.addClient <- client
	go client.listenWrite()
	client.listenRead()
}
