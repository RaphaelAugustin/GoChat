**Go Chat**

- IRC with Websocket
- Download the ``gorilla/websocket`` package with go get : ``go get github.com/gorilla/websocket``


**For use the chat:**

- launch the chat server: ```go run chat/*.go```
- launch the chat front: ```go run front.go```

**Connect to the chat**

-``` localhost:1337/public/```
