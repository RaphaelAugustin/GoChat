package main

import (
	"log"
	"net/http"
	"flag"
	"os"
	"fmt"
	"io/ioutil"
)
var addr = flag.String("addr", ":1337", "http service address")

func main() {

	indexFile, err := os.Open("public/index.html")
	if err != nil {
		fmt.Println(err)
	}
	index, err := ioutil.ReadAll(indexFile)
	if err != nil {
		fmt.Println(err)
	}

	http.Handle("/", http.FileServer(http.Dir("./")))
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, string(index))
	})
	errServer := http.ListenAndServe(*addr, nil)

	if errServer != nil {
		log.Fatal("ListenAndServe: ", errServer)
	}
}