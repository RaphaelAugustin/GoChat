$(function() {
    if (!window["WebSocket"]) {
        console.log("web Socket not work on your browser")
        return;
    }

    var subButton = $("#submitMessage");
    var msg = $("#message");
    var content = $("#content ul");
    // var msgColor = $("#textcolor");

    function appendContent(item) {
        var doScroll = content.scrollTop === content.scrollHeight - content.clientHeight;
        content.appendChild(item);
        if (doScroll) {
            content.scrollTop = content.scrollHeight - content.clientHeight;
        }
    }


    var conn = new WebSocket('ws://localhost:1374/ws');
    console.log(conn.on)
//         Textarea is editable only when socket is opened.
    conn.onopen = function(e) {
        subButton.attr("disabled", false);
    };
    conn.onclose = function(e) {
        subButton.attr("disabled", true);
        var item = document.createElement("div");
        item.innerHTML = "<b>Connection closed.</b>";
        alert("Connection closed !!")
    };


    // websocket client identifier
    var clientID
    var username


    document.getElementById("form").onsubmit = function () {
        if (!conn) {
            console.log("pas de conn")
            return false;
        }
        // console.log("msg:", msg.val())
        if (!msg.val()) {
            console.log("pas de value")
            return false;
        }

        // console.log("work")
        // var parsed_message = detectColoredMessage(msg.val());
        var messageColor = $("#textcolor").val()
        var msgObj = {
            type: "message",
            author: username,
            body: msg.val(),
//          date: Date.now()
            color: messageColor,
        };

        //check if changenickname message
        if ( msg.val().substring(0, 6) == '/nick ' ) {
            var nickname = msg.val().substring(6);
            msgObj.type = "changeNickname"
            console.log("message:",msg.val(),", nickname:", nickname)
            msgObj.author = nickname
        }

        // console.log(JSON.stringify(msgObj))

        try {
            conn.send(JSON.stringify(msgObj));
            console.log("send: Success")
        } catch (e) {
            console.log('Error : ' + e.message)
        }
        msg.val("");
        return false;
    };

    // Whenever we receive a message, update content
    conn.onmessage = function(event) {
        if (event.data != "") {

            var text = "";
            var msg = JSON.parse(event.data);
//                var time = Date.now();
//                var timeStr = time.toLocaleTimeString();
            var userlist = JSON.stringify(msg.userlist)
            userlist = JSON.parse(userlist)


            console.log(msg);

            switch (msg.type) {
                case "id":
                    clientID = msg.id;
                    break;
                case "username":
                    text = "<b>User <em>" + msg.author + "</em> signed in at " + timeStr + "</b><br>";
                    break;
                case "message":
                    text = "<li><span class='bold'>" + msg.author + "</span></b>: <span style=\"color:"+ msg.color +"\">" + msg.body + "</span></li>";
                    break;
                case "rejectusername":
                    text = "<b>Your username has been set to <em>" + msg.author + "</em> because the name you chose is in use.</b><br>"
                    break;
                case "userlist":
                    var ul = "";
                    for (var i in userlist) {
                        if (userlist.hasOwnProperty(i)) {
                            ul += "<li> <span class=\"glyphicon glyphicon-user\"></span> " + userlist[i] + "</li>";
                        }
                    }

                    setUsername(userlist)
                    document.getElementById("users").innerHTML = ul;
                    break;
            }

            if (text.length) {
                console.log(content, text)
                content.append(text);
            }
        }

//            var messages = e.data.split('\n');
//            console.log("messages:", messages)
//            for (var i = 0; i < messages.length; i++) {
//                console.log("message[I]:", messages[i])
//                var item = document.createElement("div");
//                item.innerText = messages[i];
//
//                console.log("content:",item)
//                appendContent(item);
//            };
    }

//        var timeoutId = null;
//        var typingTimeoutId = null;
//        var isTyping = false;
//
//        msg.on("keydown", function() {
//            isTyping = true;
//            window.clearTimeout(typingTimeoutId);
//        });
//
//        msg.on("keyup", function() {
//            typingTimeoutId = window.setTimeout(function() {
//                isTyping = false;
//            }, 1000);
//
//            window.clearTimeout(timeoutId);
//            timeoutId = window.setTimeout(function() {
//                if (isTyping) return;
//                conn.send(msg.val());
//            }, 1100);
//        });

    // On button click, we kill the websocket
    $('button[name="disconnect_btn"]').click(function(){
        console.log('I wanna be disconnected !');
        conn.close();
    });

    function detectColoredMessage(message) {
        var messageColor = 'black',
            newMessage = message;

        if ( message.substring(0, 6) == '/color' ) {
            newMessage = message.substring(6);
        }

        return [
            newMessage,
            messageColor
        ];
    }

    function setUsername(userlist) {
        username = userlist[clientID]
    }


    function changeNickname(message) {

            var nickname = message.substring(5);

        return nickname;
    }
})
